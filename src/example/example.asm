;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

; ---------------------------------------------------------------------------
; jump to our routine
; ---------------------------------------------------------------------------

	org $79c
	bra subOurText

; ---------------------------------------------------------------------------


 endif
 



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

; ---------------------------------------------------------------------------
; VARIABLES
; ---------------------------------------------------------------------------

TEXTPOS         equ $448C0003
NEWLINE         equ $1000000

; ---------------------------------------------------------------------------
; Execute while pause is active
; ---------------------------------------------------------------------------

subOurText:

    ; load our pointer
    lea    MyText,a1

    move.l #TEXTPOS,d5

    ; run old code
locPrintLine:

    ; load vdp command
    move.l d5,(a4)

locContinuePrinting:
    ; clear symbol
    moveq  #0,d1

    ; load next symbol
    move.b (a1)+,d1

    ; check whta to do with it
    bmi    locMinusSym
    bne    locPrintSym
    rts

locMinusSym:
    add.l  #NEWLINE,d5
    bra    locPrintLine

locPrintSym:
    move.w d1,(a5)
    bra    locContinuePrinting


; ---------------------------------------------------------------------------
MyText:
    dc.b " this is an example project{",$ff
    dc.b "use it to create all sorts of",$ff
    dc.b "  romhacks for ",$7c,$7d,$7e,$7f," games{",$ff
    dc.b "     visit  segaman{top",$ff
    dc.b "       for more info",$0
; ---------------------------------------------------------------------------

 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
