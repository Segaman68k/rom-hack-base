@echo off
cls

rem ------------------------------------
rem Setup CMD
rem ------------------------------------

set USEANSI=n

rem ------------------------------------
rem Compile
rem ------------------------------------

set AS_MSGPATH=bin/asmsg
bin\asw  -xx -c -A src/project.asm

rem ------------------------------------
rem Check issues
rem ------------------------------------

IF NOT EXIST src/project.p goto LABLPAUSE
IF EXIST src/project.p goto Convert2ROM

rem ------------------------------------
rem Build successful
rem ------------------------------------

:Convert2ROM

set AS_MSGPATH=bin/msg

rem create "out" folder
if not exist "out" mkdir "out"

rem pack into rom image
bin\p2bin src/project.p out\project.bin -l 0 -r $-$

rem fix contents to able to write on a cart
bin\rompad.exe out\project.bin 255 0

rem fix checksum
bin\fixheadr.exe out\project.bin

rem create patch
bin\flips.exe -c -i data\rom.bin out\project.bin

rem remove garbage
del src\project.p
del src\project.h

rem done
exit /b

rem ------------------------------------
rem Build unsuccessful
rem ------------------------------------

:LABLPAUSE

rem wait to notice user
pause

rem done
exit /b

rem ------------------------------------
